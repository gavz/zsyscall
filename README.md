# zsyscall

This is my implementation of the [Hell's Gate VX technique](https://github.com/am0nsec/HellsGate).

The main difference with the original implementation is the use of the `zsyscall` procedure instead of `HellsGate` and `HellDescent` for using syscalls.

So instead of using:
```c
HellsGate(syscall_nummber);
HellDescent(arg1, arg2, arg3);
```

You can use:
```c
zsyscall(syscall_number, arg1, arg2, arg3);
```

The zsyscall procedure implementation is based on linux [syscall](https://sourceware.org/git/?p=glibc.git;a=blob;f=sysdeps/unix/sysv/linux/x86_64/syscall.S;h=192455a89883fc815ecb58677c0da6a7bb4583fa;hb=HEAD) function.

## Usage

The tool lists the syscall numbers and offers a couple of examples of using syscalls:

- `zsyscall.exe list`: List ntdll functions and their respective syscalls numbers.
- `zsyscall.exe sleep <millis>`: Sleeps the given milliseconds by using a syscall.
- `zsyscall.exe file <filename>`: Create a file by using a syscall.

## More information

You can learn more about the technique in the paper https://vxug.fakedoma.in/papers/VXUG/Exclusive/HellsGate.pdf .

The author of this implementation is Eloy Pérez González (@zer1t0).
