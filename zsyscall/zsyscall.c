
#include "structs.h"
#include <Windows.h>
#include <winnt.h>
#include <stdio.h>


extern int zsyscall(WORD syscall_number, ...);

PTEB get_TEB() {
//#if _WIN64
	return (PTEB)__readgsqword(0x30);
//#else
//	return (PTEB)__readfsdword(0x16);
//#endif
}

PPEB get_PEB() {
	PPEB peb = NULL;
	PTEB teb = NULL;

	teb = get_TEB();
	if (!teb) {
		return NULL;
	}

	peb = teb->ProcessEnvironmentBlock;
	return peb;
	/*
	// Alternatively you can get the PEB this way
	#ifdef _WIN64
		return (PPEB)__readgsqword(0x60); //64bit process
	#else
		return (PPEB)__readfsdword(0x30); //32bit process
	#endif
	*/
}

// ntdll.dll
#define NTDLL_DJB2 0x11c9b04d
#define SLEEP_DJB2 0xa49084a
#define CREATE_FILE_DJB2 0x15a5ecdb
#define CLOSE_HANDLE_DJB2  0x8b8e133d

// Based on http://www.cse.yorku.ca/~oz/hash.html 
unsigned long djb2(const unsigned char* bs, size_t size) {
	unsigned long hash = 5381;
	unsigned char c = 0;
	size_t i = 0;

	for (i = 0; i < size; i++) {
		c = *bs++;
		hash = ((hash << 5) + hash) + c;
	}

	return hash;
}

unsigned long djb2s(const char* str) {
	return djb2(str, strlen(str));
}

unsigned long djb2w(LPCWSTR str) {
	return djb2((const unsigned char *) str, wcslen(str) * 2);
}

PLDR_MODULE get_module(unsigned long module_hash) {
	PLIST_ENTRY first_entry = NULL;
	PLIST_ENTRY entry = NULL;
	PLDR_MODULE module = NULL;
	PPEB peb = NULL;

	peb = get_PEB();
	if (!peb) {
		return NULL;
	}

	first_entry = &(peb->LoaderData->InLoadOrderModuleList);
	for (entry = first_entry->Flink; entry != first_entry; entry = entry->Flink) {
		module = (PLDR_MODULE)entry;

		if (djb2w(module->BaseDllName.Buffer) == module_hash) {
			return module;
		}
	}

	return module;
}

PIMAGE_EXPORT_DIRECTORY get_export_directory(PBYTE module_base) {
	PIMAGE_OPTIONAL_HEADER opt_hdr = NULL;
	PIMAGE_DOS_HEADER dos_hdr = NULL;
	PIMAGE_FILE_HEADER file_hdr = NULL;
	PIMAGE_NT_HEADERS nt_hdrs = NULL;

	dos_hdr = (PIMAGE_DOS_HEADER)module_base;
	if (dos_hdr->e_magic != IMAGE_DOS_SIGNATURE) {
		return NULL;
	}

	nt_hdrs = (PIMAGE_NT_HEADERS)(module_base + dos_hdr->e_lfanew);
	if (nt_hdrs->Signature != IMAGE_NT_SIGNATURE) {
		return NULL;
	}

	file_hdr = (PIMAGE_FILE_HEADER)(module_base + (dos_hdr->e_lfanew + sizeof(DWORD)));
	opt_hdr = (PIMAGE_OPTIONAL_HEADER)((PBYTE)file_hdr + sizeof(IMAGE_FILE_HEADER));

	return (PIMAGE_EXPORT_DIRECTORY)(
		module_base +
		opt_hdr->DataDirectory[IMAGE_DIRECTORY_ENTRY_EXPORT].VirtualAddress
		);
}

#define MOV_EAX_OPCODE 0xB8
/*
 The functions with syscall starts with:
 4c8bd1     mov r10, rcx
 b8xxxxxxxx mov eax, x <- syscall_number
*/
BOOL contains_syscall(PBYTE func_addr) {
	return *(func_addr) == 0x4c
		&& *(func_addr + 1) == 0x8b
		&& *(func_addr + 2) == 0xd1
		&& *(func_addr + 3) == 0xb8;
}

void print_syscalls(PLDR_MODULE ntdll_module) {
	PIMAGE_EXPORT_DIRECTORY export_dir = NULL;
	PBYTE mod_base = ntdll_module->BaseAddress;
	PDWORD addr_functions = NULL;
	PDWORD addr_names = NULL;
	PWORD addr_name_ordinals = NULL;
	WORD syscall_number = 0;
	DWORD i = 0;
	char* func_name = NULL;
	PBYTE func_addr = NULL;

	export_dir = get_export_directory(mod_base);

	addr_functions = (PDWORD)(mod_base + export_dir->AddressOfFunctions);
	addr_names = (PDWORD)(mod_base + export_dir->AddressOfNames);
	addr_name_ordinals = (PWORD)(mod_base + export_dir->AddressOfNameOrdinals);

	for (i = 0; i < export_dir->NumberOfNames; i++) {
		func_name = mod_base + addr_names[i];
		func_addr = mod_base + addr_functions[addr_name_ordinals[i]];

		if (contains_syscall(func_addr)) {
			syscall_number = *(PWORD)(func_addr + 4);
			printf("%s -> 0x%x %u\n", func_name, syscall_number, syscall_number);
		}
	}
}

BOOL get_syscall(PLDR_MODULE ntdll_module, DWORD func_hash, PWORD syscall_number) {
	PIMAGE_EXPORT_DIRECTORY export_dir = NULL;
	PBYTE mod_base = ntdll_module->BaseAddress;
	PDWORD addr_functions = NULL;
	PDWORD addr_names = NULL;
	PWORD addr_name_ordinals = NULL;
	DWORD i = 0;
	char* func_name = NULL;
	PBYTE func_addr = NULL;


	export_dir = get_export_directory(mod_base);

	addr_functions = (PDWORD)(mod_base + export_dir->AddressOfFunctions);
	addr_names = (PDWORD)(mod_base + export_dir->AddressOfNames);
	addr_name_ordinals = (PWORD)(mod_base + export_dir->AddressOfNameOrdinals);

	for (i = 0; i < export_dir->NumberOfNames; i++) {
		func_name = mod_base + addr_names[i];
		func_addr = mod_base + addr_functions[addr_name_ordinals[i]];

		if (contains_syscall(func_addr) && djb2s(func_name) == func_hash) {
			*syscall_number = *(PWORD)(func_addr + 4);
			return TRUE;
		}
	}

	return FALSE;
}

int list_syscalls() {
	PLDR_MODULE ntdll_module = NULL;
	ntdll_module = get_module(NTDLL_DJB2);
	if (!ntdll_module) {
		fprintf(stderr, "Unable to find Ntdll.dll\n");
		return -1;
	}

	print_syscalls(ntdll_module);
	return 0;
}

int exec_sleep_syscall(LONGLONG milliseconds) {
	WORD sleep_syscall = 0;
	PLDR_MODULE ntdll_module = NULL;
	LARGE_INTEGER nano_neg = { 0 };
	ntdll_module = get_module(NTDLL_DJB2);
	if (!ntdll_module) {
		fprintf(stderr, "Unable to find Ntdll.dll\n");
		return -1;
	}

	if (!get_syscall(ntdll_module, SLEEP_DJB2, &sleep_syscall)) {
		fprintf(stderr, "Unable to find the syscall for sleep\n");
		return -1;
	};

	printf("0x%x\n", sleep_syscall);

	nano_neg.QuadPart = -(milliseconds * 10000);
	zsyscall(sleep_syscall, 0, &nano_neg);

	return 0;
}

typedef struct _IO_STATUS_BLOCK {
	union {
		NTSTATUS Status;
		PVOID    Pointer;
	};
	ULONG_PTR Information;
} IO_STATUS_BLOCK, * PIO_STATUS_BLOCK;

int exec_create_file_syscall(wchar_t* name) {
	HANDLE file_handle = NULL;
	DWORD desired_access = GENERIC_WRITE;
	IO_STATUS_BLOCK io_status = { 0 };
	OBJECT_ATTRIBUTES attrs = { 0 };
	ULONG share_access = 0;
	PLARGE_INTEGER allocation_size = NULL;
	ULONG file_attributes = FILE_ATTRIBUTE_NORMAL; 
	ULONG creation_dispostion = CREATE_ALWAYS;
	ULONG create_options = 0;
	PVOID ea_buffer = NULL;
	ULONG ea_len = 0;
	WORD create_file_syscall = 0;
	WORD close_handle_syscall = 0;
	PLDR_MODULE ntdll_module = NULL;
	UNICODE_STRING object_name = { 0 };
	unsigned long djb2_hash = 0;

	object_name.Buffer = name;
	object_name.Length = (USHORT)(wcslen(name) * 2);
	object_name.MaximumLength = object_name.Length;
	
	attrs.Length = 0x30;
	attrs.RootDirectory = (PVOID)0x44;
	attrs.Attributes = 0x40;
	attrs.SecurityDescriptor = NULL;
	attrs.SecurityQualityOfService = NULL;
	attrs.ObjectName = &object_name;

	ntdll_module = get_module(NTDLL_DJB2);
	if (!ntdll_module) {
		fprintf(stderr, "Unable to find Ntdll.dll\n");
		return -1;
	}

	if (!get_syscall(ntdll_module, CREATE_FILE_DJB2, &create_file_syscall)) {
		fprintf(stderr, "Unable to find the syscall for create file\n");
		return -1;
	};

	zsyscall(create_file_syscall,
		&file_handle,
		desired_access,
		&attrs,
		&io_status,
		allocation_size,
		file_attributes,
		share_access,
		creation_dispostion,
		create_options,
		ea_buffer,
		ea_len
	);

	/*
	// The equivalent call would be
	file_handle = CreateFileW(name,    // name of the file
		desired_access, // open for writing
		share_access,             // sharing mode, none in this case
		0,             // use default security descriptor
		creation_dispostion, // overwrite if exists
		file_attributes,
		0);
	*/
	if (!file_handle)
	{
		fprintf(stderr, "CreateFile() failed: %u\n", GetLastError());
		return -1;
	}

	if (!get_syscall(ntdll_module, CLOSE_HANDLE_DJB2, &close_handle_syscall)) {
		fprintf(stderr, "Unable to find the syscall for close handle\n");
		return -1;
	};

	printf("CreateFile() succeeded\n");
	zsyscall(close_handle_syscall, file_handle);
	return 0;
}

#define ACTION_LIST 1
#define ACTION_SLEEP 2
#define ACTION_FILE 3

int wmain(int argc, wchar_t** argv)
{
	WORD action = 0;
	LONGLONG sleep_milliseconds = 0;
	wchar_t* name = NULL;

	if (argc == 1) {
		printf("Usage:\n");
		printf("List syscalls : %ws list\n", argv[0]);
		printf("Sleep with syscall : %ws sleep <millis>\n", argv[0]);
		printf("Create file with syscall : %ws file <filename>\n", argv[0]);
		return 0;
	}
	else if (argc > 1) {
		if (!wcscmp(argv[1], L"list")) {
			action = ACTION_LIST;
		}
		else if (!wcscmp(argv[1], L"sleep")) {
			action = ACTION_SLEEP;
			if (argc == 2) {
				fprintf(stderr, "Sleep requires an interval in milliseconds\n");
				return -1;
			}
			sleep_milliseconds = _wtoi(argv[2]);
		}
		else if (!wcscmp(argv[1], L"file")) {
			action = ACTION_FILE;
			if (argc == 2) {
				fprintf(stderr, "file requires an filename\n");
				return -1;
			}
			name = argv[2];
		}
		else {
			fprintf(stderr, "Unknown action %ws\n", argv[1]);
			return -1;
		}
	}

	switch (action) {
	case ACTION_LIST:
		return list_syscalls();
		break;
	case ACTION_SLEEP:
		return exec_sleep_syscall(sleep_milliseconds);
		break;
	case ACTION_FILE:
		return exec_create_file_syscall(name);
		break;
	}

	return 0;
}


