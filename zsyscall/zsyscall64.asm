; Zsyscall
; syscall for Windows
; 
; by zer1t0

.code

	zsyscall PROC
		;; 1. Set the all the registers to their correct value for the syscall
		; EAX must be set to the syscall number 
		xor rax, rax
		mov eax, ecx

		; Now all the argument registers need to move its value 
		; to the previous one
		; R10 is used instead of RCX since this one is modified for syscall
		mov r10, rdx
		mov rdx, r8
		mov r8, r9
		mov r9, [rsp+28h]
		
		;; 2. Remove the first argument from stack
		; The intention is move all the arguments to the previous 
		; position so we just need to move the stack upwards.
		; We need to copy the return address to the previous position,
		; where is the placeholder of the rcx, which is useless and 
		; in any case contains the syscall number which we want to 
		; get rid of, so we are good. 
		; We need to copy the return address in case the syscall modifies 
		; the stack with local variables and we lost its value.
		mov r11, [rsp]
		add rsp, 8h
		mov [rsp], r11
		
		;; 3. Execute syscall
		syscall

		;; 4. Restore the return address and stack position
		mov r11, [rsp]
		sub rsp, 8h
		mov [rsp], r11

		ret
	zsyscall ENDP 

end
